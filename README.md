# Portfolio Architecture Tooling

This project contains customized installation for drawio tooling used by Portfolio Architecture team.

Available online here: https://www.redhat.com/architect/portfolio/tool/index.html
DEV: https://dev.osspa.org/architect/portfolio/tool/index.html

## Releases

2.2 - Updated icons to latest brand set.  
    - Added some new icons.  

2.0 - Adopted new brand colors and icons.  
    - Improved network and data linking functionality in schematic diagrams.  
    - Automatically colored detail and logical elements.  
    - Resizeable regulr and detail element for logical diagrams.  

1.0 - Portfolio architecture tooling with basic icon set for logical, schematic, detail diagrams templates.  

Download the libraries for this version and manually import them into draw.io or your local draw.io application.  
**NOTE:** Downloads below will be saved locally with a filename extension of ".txt" They will still be importable to local versions of draw.io regardless of extension.

Icon Libraries
* [Application Icons](https://gitlab.com/osspa/osspa-tool/-/raw/main/Libraries/Application%20Icons?inline=false)  
* [Application Services Icons](https://gitlab.com/osspa/osspa-tool/-/raw/main/Libraries/Application%20Services%20Icons?inline=false)  
* [Infrastructure Icons](https://gitlab.com/osspa/osspa-tool/-/raw/main/Libraries/Infrastructure%20Icons?inline=false)  
* [Service Icons](https://gitlab.com/osspa/osspa-tool/-/raw/main/Libraries/Service%20Icons?inline=false)  
[Logical Diagram Library](https://gitlab.com/osspa/osspa-tool/-/raw/main/Libraries/Logical%20Diagrams?inline=false)  
[Detail Diagram Library](https://gitlab.com/osspa/osspa-tool/-/raw/main/Libraries/Detail%20Diagrams?inline=false)  
[Schematic Diagram Library](https://gitlab.com/osspa/osspa-tool/-/raw/main/Libraries/Schematic%20Diagrams?inline=false)  

To load the files, go to "File -> Import from -> Device..." and chose the local library file.  

**NOTE:**  You may need to look for all files types in order to see the files for import.
