#git clone -b prod https://gitlab.com/osspa/portfolio-architecture-tooling.git


echo "Pull entire TOOL from GIT"
echo "-------------------------------------------------------------------------"


echo "-------Getting DEV from GIT---------"
cd ~/dev
echo "Remove previous workspace"
rm -rf portfolio-architecture-tooling

echo "Checkout main branch"
git clone https://gitlab.com/osspa/portfolio-architecture-tooling.git

cd portfolio-architecture-tooling
echo "Replace variable to dev environment"
mv index.html index.html.bak
mv template_links template_links.bak
sed 's/<TOOL_HOST>/tool.osspa.org/g' index.html.bak | sed 's/<BRANCH>/dev%2F/g' | sed 's/<DRAW_HOST>/diag.osspa.org/g' > index.html
sed 's/<TOOL_HOST>/tool.osspa.org/g' template_links.bak | sed 's/<BRANCH>/dev%2F/g' > template_links
echo "Remove unwanted files"
rm index.html.bak
rm template_links.bak


echo "-------Getting DEV from GIT---------"
cd ~/prod
echo "Remove previous workspace"
rm -rf portfolio-architecture-tooling

echo "Checkout prod branch"
git clone -b prod https://gitlab.com/osspa/portfolio-architecture-tooling.git
sed 's/<TOOL_HOST>/tool.osspa.org/g' index.html.bak | sed 's/<BRANCH>//g' | sed 's/<DRAW_HOST>/diag.osspa.org/g' > index.html
sed 's/<TOOL_HOST>/tool.osspa.org/g' template_links.bak | sed 's/<BRANCH>//g' > template_links


echo "------Move all dev code under dev sub-directory"
cp ~/dev/portfolio-architecture-tooling/* ~/prod/portfolio-architecture-tooling/dev


podman login --authfile ~/.podmanauth quay.io
echo ""
echo "Build pawebsite from PROD branch"
echo "-------------------------------------------------------------------------"
podman build . -t tool:latest



echo ""
echo "Start DEV toll Server"
podman stop tool
podman rm tool
podman run -dit -p 8083:80 --name="tool" osspa/tool:latest

cd ~/ci
