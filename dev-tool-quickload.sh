#!/bin/sh
#git clone -b prod https://gitlab.com/osspa/portfolio-architecture-tooling.git



echo "-------Getting DEV from GIT---------"
cd ~/dev
echo "Remove previous workspace"
rm -rf osspa-tool


echo "Checkout prod branch"
git clone https://gitlab.com/osspa/osspa-tool.git
cd osspa-tool

cp -R images/ ../osspa-site/asset/architect/portfolio/tool/
cp -R Libraries/ ../osspa-site/asset/architect/portfolio/tool/
cp -R Templates/ ../osspa-site/asset/architect/portfolio/tool/
cp -R index.html ../osspa-site/asset/architect/portfolio/tool/


cd ~/ci
